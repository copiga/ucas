#include <stdio.h>
#include <ctype.h>

void afterinit(void);
int BTECL3(char type);
//int GCEAS(void);
int keyskills(int level);



int pointstotal;

int main(void)
{
  pointstotal = 0;    /*initialisation is good...*/
  afterinit();   /*main is only to initialise and clean up...*/
  printf("\nfinal points calculation = %d\n", pointstotal);
  return 0;
}

void afterinit(void)
{
  printf("\npoints so far: %d\n",pointstotal);
  
    char ch;
  
  int choice=0;
  puts("please choose what type of qualification you wish to add or press \'q\' to quit");
  puts("");
  puts("1. BTEC L3 award");
  puts("2. BTEC L3 certificate");
  puts("3. BTEC L3 diploma");
  //puts("4. GCE AS"); maybe later, difficult to discern exaxctly where this is on the tariff...
  //puts("5. GCE A2"); ''
  puts("6. key skills L2");
  puts("7. key skills L3");
  puts("8. key skills L4");

  
    while((ch=getchar())!='\n')
    {
      if(ch=='q'){return ;}
      choice = choice*10;
      choice += atoi(&ch);
    }
    
  switch(choice)
    {
    case 1:
      pointstotal += BTECL3('a');
      break;
    case 2:
      pointstotal += BTECL3('c');
      break;
    case 3:
      pointstotal += BTECL3('d');
      break;
    case 6:
      pointstotal += keyskills(2);
      break;
    case 7:
      pointstotal += keyskills(3);
      break;
    case 8:
      pointstotal += keyskills(4);
      break;
      
    }
  afterinit();/*continue this loop! indefinitely...*/
}

int BTECL3(char type)
{
  char grade[2];
  int points = 0;
  if(type=='a')
    {
      puts("please enter what grade you have/expect\n");
      grade[0]=toupper(getchar());
      if(grade[0]=='D')
	{
	  points = 120;
	}
      else if(grade[0]=='M')
	{
	  points = 80;
	}
      else if(grade[0]=='p')
	{
	  points = 40;
	}
    }
  else if(type == 'c')
    {
      puts("please enter what grade you have/expect\n");
      grade[0]=toupper(getchar());
      grade[1]=toupper(getchar());
      points += (grade[0]=='D'?120:grade[0]=='M'?80:40); /*UGLY but small, yes?*/
      points += (grade[1]=='D'?120:grade[1]=='M'?80:40); /*UGLY but small, yes?*/
    }
  else if(type == 'd')
    {
      puts("please enter what grade you have/expect\n");
      grade[0]=toupper(getchar());
      grade[1]=toupper(getchar());
      grade[2]=toupper(getchar());
      points += (grade[0]=='D'?120:grade[0]=='M'?80:40); /*UGLY but small, yes?*/
      points += (grade[1]=='D'?120:grade[1]=='M'?80:40); /*UGLY but small, yes?*/
      points += (grade[2]=='D'?120:grade[2]=='M'?80:40); /*UGLY but small, yes?*/
    }
  return points;
}

int keyskills(int level)
{
  int points = 0;
  if(level == 2)
    {
      puts("did you pass? Y|n");
      if(toupper(getchar())== 'Y')
	{
	  points += 10;
	}
    }
  else if(level == 3)
    {
      puts("did you pass? Y|n");
      if(toupper(getchar())== 'Y')
	{
	  points += 20;
	}
    }
  else if(level == 4)
    {
      puts("did you pass? Y|n");
      if(toupper(getchar())=='Y')
	{
	  points += 30;
	}
    }
  return points;
}
